﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim L = 4
        ValorL.Text = L
        Dim matrizIni(L - 1, L - 1) As String
        Dim count As Integer = 1
        Dim Row As String

        Resultado.Items.Clear()
        For i As Integer = 0 To L - 1
            For j As Integer = 0 To L - 1
                matrizIni(i, j) = count
                count = count + 1
            Next
        Next

        For i As Integer = 0 To L - 1
            Row = ""
            For j As Integer = 0 To L - 1
                Row = Row + matrizIni(i, j).ToString() + vbTab
            Next
            Resultado.Items.Add(Row)
        Next
    End Sub

    Private Sub Ejercicio3_Click(sender As Object, e As EventArgs) Handles Ejercicio3.Click
        Dim L = ValorL.Text
        If L > 0 Then
            Dim matrizIni(L - 1, L - 1) As String
            Dim count As Integer = 1
            Dim Suma As Integer
            Dim Row As String

            Resultado.Items.Clear()
            For i As Integer = 0 To L - 1
                For j As Integer = 0 To L - 1
                    Suma = i + j
                    If (Suma < L - 1) Then
                        matrizIni(i, j) = 0
                    Else
                        matrizIni(i, j) = count
                        count = count + 1
                    End If
                Next
            Next

            For i As Integer = 0 To L - 1
                Row = ""
                For j As Integer = 0 To L - 1
                    Row = Row + matrizIni(i, j).ToString() + vbTab
                Next
                Resultado.Items.Add(Row)
            Next
        End If
    End Sub

    Private Sub BEjercicio4_Click(sender As Object, e As EventArgs) Handles Ejercicio4.Click
        Dim L = ValorL.Text
        If L > 0 Then
            Dim matrizIni(L - 1, L - 1) As String
            Dim count As Integer = L * L
            Dim Row As String

            Resultado.Items.Clear()
            For i As Integer = 0 To L - 1
                If i Mod 2 = 0 Then
                    For j As Integer = L - 1 To 0 Step -1
                        matrizIni(j, i) = count
                        count = count - 1
                    Next
                Else
                    For j As Integer = 0 To L - 1
                        matrizIni(j, i) = count
                        count = count - 1
                    Next
                End If
            Next


            For i As Integer = 0 To L - 1
                Row = ""
                For j As Integer = 0 To L - 1
                    Row = Row + matrizIni(i, j).ToString() + vbTab
                Next
                Resultado.Items.Add(Row)
            Next
        End If
    End Sub
End Class
