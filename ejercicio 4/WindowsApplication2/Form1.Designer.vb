﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Resultado = New System.Windows.Forms.ListBox()
        Me.Ejercicio4 = New System.Windows.Forms.Button()
        Me.Ejercicio3 = New System.Windows.Forms.Button()
        Me.ValorL = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Resultado
        '
        Me.Resultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.25!)
        Me.Resultado.FormattingEnabled = True
        Me.Resultado.HorizontalScrollbar = True
        Me.Resultado.ItemHeight = 26
        Me.Resultado.Location = New System.Drawing.Point(86, 149)
        Me.Resultado.Name = "Resultado"
        Me.Resultado.ScrollAlwaysVisible = True
        Me.Resultado.Size = New System.Drawing.Size(623, 316)
        Me.Resultado.TabIndex = 0
        '
        'Ejercicio4
        '
        Me.Ejercicio4.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Ejercicio4.Location = New System.Drawing.Point(467, 86)
        Me.Ejercicio4.Name = "Ejercicio4"
        Me.Ejercicio4.Size = New System.Drawing.Size(141, 35)
        Me.Ejercicio4.TabIndex = 1
        Me.Ejercicio4.Text = "Ejercicio 4"
        Me.Ejercicio4.UseVisualStyleBackColor = True
        '
        'Ejercicio3
        '
        Me.Ejercicio3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Ejercicio3.Location = New System.Drawing.Point(179, 86)
        Me.Ejercicio3.Name = "Ejercicio3"
        Me.Ejercicio3.Size = New System.Drawing.Size(141, 35)
        Me.Ejercicio3.TabIndex = 2
        Me.Ejercicio3.Text = "Ejercicio 3"
        Me.Ejercicio3.UseVisualStyleBackColor = True
        '
        'ValorL
        '
        Me.ValorL.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.ValorL.Location = New System.Drawing.Point(467, 27)
        Me.ValorL.Name = "ValorL"
        Me.ValorL.Size = New System.Drawing.Size(141, 32)
        Me.ValorL.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Label1.Location = New System.Drawing.Point(144, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(294, 26)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Número de Filas y Columnas"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(801, 493)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ValorL)
        Me.Controls.Add(Me.Ejercicio3)
        Me.Controls.Add(Me.Ejercicio4)
        Me.Controls.Add(Me.Resultado)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Resultado As ListBox
    Friend WithEvents Ejercicio4 As Button
    Friend WithEvents Ejercicio3 As Button
    Friend WithEvents ValorL As TextBox
    Friend WithEvents Label1 As Label
End Class
