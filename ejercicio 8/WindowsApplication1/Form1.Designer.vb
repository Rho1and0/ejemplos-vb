﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBoxN = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxX = New System.Windows.Forms.TextBox()
        Me.TextBoxRes = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBoxN
        '
        Me.TextBoxN.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.TextBoxN.Location = New System.Drawing.Point(524, 87)
        Me.TextBoxN.Name = "TextBoxN"
        Me.TextBoxN.Size = New System.Drawing.Size(148, 29)
        Me.TextBoxN.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.Label2.Location = New System.Drawing.Point(451, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Índice"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(52, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Valor de X"
        '
        'TextBoxX
        '
        Me.TextBoxX.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.TextBoxX.Location = New System.Drawing.Point(163, 87)
        Me.TextBoxX.Name = "TextBoxX"
        Me.TextBoxX.Size = New System.Drawing.Size(152, 29)
        Me.TextBoxX.TabIndex = 1
        '
        'TextBoxRes
        '
        Me.TextBoxRes.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.TextBoxRes.Location = New System.Drawing.Point(335, 236)
        Me.TextBoxRes.Name = "TextBoxRes"
        Me.TextBoxRes.Size = New System.Drawing.Size(152, 29)
        Me.TextBoxRes.TabIndex = 5
        Me.TextBoxRes.UseWaitCursor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.Label3.Location = New System.Drawing.Point(234, 239)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 24)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Resultado"
        Me.Label3.UseWaitCursor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Highlight
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button1.Location = New System.Drawing.Point(284, 152)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(164, 44)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Calcular"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.ClientSize = New System.Drawing.Size(758, 339)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBoxRes)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxN)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxX)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBoxN As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxX As TextBox
    Friend WithEvents TextBoxRes As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button1 As Button
End Class
