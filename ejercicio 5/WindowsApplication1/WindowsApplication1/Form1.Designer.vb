﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Autor = New System.Windows.Forms.TextBox()
        Me.Titulo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Anho = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Codigo = New System.Windows.Forms.TextBox()
        Me.Generar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Label1.Location = New System.Drawing.Point(198, 47)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Autor"
        '
        'Autor
        '
        Me.Autor.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Autor.Location = New System.Drawing.Point(372, 45)
        Me.Autor.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Autor.Name = "Autor"
        Me.Autor.Size = New System.Drawing.Size(213, 32)
        Me.Autor.TabIndex = 1
        '
        'Titulo
        '
        Me.Titulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Titulo.Location = New System.Drawing.Point(372, 118)
        Me.Titulo.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Titulo.Name = "Titulo"
        Me.Titulo.Size = New System.Drawing.Size(213, 32)
        Me.Titulo.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Label2.Location = New System.Drawing.Point(198, 120)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 26)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Título"
        '
        'Anho
        '
        Me.Anho.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Anho.Location = New System.Drawing.Point(372, 197)
        Me.Anho.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Anho.Name = "Anho"
        Me.Anho.Size = New System.Drawing.Size(213, 32)
        Me.Anho.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Label3.Location = New System.Drawing.Point(198, 200)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(165, 26)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Año de Edición "
        '
        'Codigo
        '
        Me.Codigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Codigo.Location = New System.Drawing.Point(248, 343)
        Me.Codigo.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Codigo.Name = "Codigo"
        Me.Codigo.Size = New System.Drawing.Size(287, 32)
        Me.Codigo.TabIndex = 6
        Me.Codigo.Text = "Código"
        '
        'Generar
        '
        Me.Generar.Location = New System.Drawing.Point(299, 281)
        Me.Generar.Name = "Generar"
        Me.Generar.Size = New System.Drawing.Size(155, 39)
        Me.Generar.TabIndex = 7
        Me.Generar.Text = "Generar"
        Me.Generar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(791, 450)
        Me.Controls.Add(Me.Generar)
        Me.Controls.Add(Me.Codigo)
        Me.Controls.Add(Me.Anho)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Titulo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Autor)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Autor As TextBox
    Friend WithEvents Titulo As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Anho As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Codigo As TextBox
    Friend WithEvents Generar As Button
End Class
